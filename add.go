package main

import (
	"crypto"
	_ "crypto/md5"
	_ "crypto/sha1"
	_ "crypto/sha256"
	_ "crypto/sha512"
	"fmt"
	"hash"
	"labix.org/v2/mgo/bson"
	"os"
	"path/filepath"
)

type sampleFile struct {
	path     string
	fileInfo os.FileInfo

	digests map[string]hash.Hash
}

func add(args []string) {
	badPath := false
	sampleFiles := make(map[string]sampleFile)

	// Collect a list of all of the files
	for _, arg := range args {
		globPaths, err := filepath.Glob(arg)
		if err != nil {
			fmt.Printf("An error occurred while listing files\n  %s\n", err.Error())
			return
		}

		if len(globPaths) == 0 {
			fmt.Printf("%s: file not found\n", arg)
			badPath = true
			continue
		}

		for _, path := range globPaths {
			if _, contains := sampleFiles[path]; !contains {
				fileInfo, err := os.Stat(path)
				if err != nil {
					fmt.Printf("%s: %s\n", path, err.Error())
					badPath = true
					continue
				}

				sf := sampleFile{
					path:     path,
					fileInfo: fileInfo,
					digests:  make(map[string]hash.Hash),
				}
				sampleFiles[path] = sf
			}
		}
	}

	if badPath {
		return
	}

	// Load and digest all of the files
	dataLen := int64(65536)
	data := make([]byte, dataLen)
	for _, sf := range sampleFiles {
		// Digest the file
		sf.digests["md5"] = crypto.MD5.New()
		sf.digests["sha1"] = crypto.SHA1.New()
		sf.digests["sha256"] = crypto.SHA256.New()

		println(sf.path)

		f, err := os.Open(sf.path)
		if err != nil {
			fmt.Printf("   Failed to open file: %s\n", sf.path, err.Error())
			return
		}
		defer f.Close()

		for i := int64(0); i < sf.fileInfo.Size(); i += dataLen {
			fmt.Printf("\r   [% 3d%%] Digesting...", int(float64(i)/float64(sf.fileInfo.Size())*100.0))

			n, err := f.Read(data)
			if err != nil {
				fmt.Printf("\n   Failed to read file: %s\n", err.Error())
				return
			}

			for _, digest := range sf.digests {
				digest.Write(data[:n])
			}
		}

		// Check the database
		fmt.Printf("\r   Checking database...")

		digests := []bson.M{}
		filterDigests := []bson.M{}
		for digestType, digest := range sf.digests {
			digests = append(digests, bson.M{
				"type":   digestType,
				"digest": fmt.Sprintf("%02x", digest.Sum(nil)),
			})
			filterDigests = append(filterDigests, bson.M{
				"digests.type":   digestType,
				"digests.digest": fmt.Sprintf("%02x", digest.Sum(nil)),
			})
		}

		filter := bson.M{"$or": filterDigests}
		count, err := samplesCol.Find(filter).Count()
		if err != nil {
			fmt.Printf("\n   Failed to query database: %s\n", err.Error())
			return
		}

		if count > 0 {
			fmt.Printf("\r   Sample already exists in database\n")
			continue
		}

		// Write the file to GridFS
		gf, err := samplesFS.Create("")
		if err != nil {
			fmt.Printf("\n   Failed to create file in database: %s\n", err.Error())
			return
		}
		defer gf.Close()

		f.Seek(0, os.SEEK_SET)
		for i := int64(0); i < sf.fileInfo.Size(); i += dataLen {
			fmt.Printf("\r   [% 3d%%] Importing...", int(float64(i)/float64(sf.fileInfo.Size())*100.0))

			n, err := f.Read(data)
			if err != nil {
				fmt.Printf("\n   Failed to read file: %s\n", err.Error())
				return
			}

			dataToWrite := data[:n]
			for len(dataToWrite) > 0 {
				n, err = gf.Write(dataToWrite)
				if err != nil {
					fmt.Printf("\n   Failed to write file: %s\n", err.Error())
					return
				}

				dataToWrite = dataToWrite[n:]
			}
		}

		sample := bson.M{
			"fileId":  gf.Id(),
			"digests": digests,
		}
		err = samplesCol.Insert(sample)
		if err != nil {
			fmt.Printf("\n   Failed to insert sample into database: %s\n", err.Error())
			return
		}

		fmt.Printf("\r   New sample imported successfully\n")
	}
}
