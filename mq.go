package main

import (
	"fmt"
	"labix.org/v2/mgo"
	"os"
)

var (
	settings map[string]interface{}

	session    *mgo.Session
	samplesCol *mgo.Collection
	samplesFS  *mgo.GridFS
)

func init() {
	settings = make(map[string]interface{})

	settings["connectionURL"] = "mongodb://localhost"
	settings["databaseName"] = "malware"
}

func main() {
	session, err := mgo.Dial(settings["connectionURL"].(string))
	if err != nil {
		fmt.Printf("An error occurred while connecting to the database\n  %s\n", err.Error())
		return
	}
	defer session.Close()

	session.SetSafe(&mgo.Safe{})

	db := session.DB(settings["databaseName"].(string))
	samplesCol = db.C("samples")
	samplesFS = db.GridFS("samples")

	if len(os.Args) > 1 {
		if os.Args[1] == "add" {
			add(os.Args[2:])
		}
	}
}
